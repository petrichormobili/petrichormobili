---
title: Hakkımızda
description: 
date: ''
aliases:
  - about-us
  - about-hugo
  - contact

menu:
    main: 
        weight: -90
        params:
            icon: user
---

Mobilya sektöründe en iyi kalite ve estetiği tasarlayarak sunan **Petrichor Mobili** siz değerli müşterilerimizin hizmetindedir. Yatak odası, yemek odası, koltuk takımı, tv ünitesi konusunda faaliyetlerini yürüten firmamız sağlam, kaliteli, çağdaş, modern, en kaliteli, en üstün ürünleri ve en son trend modelleri, en uygun koşullarda siz değerli müşterilerimize sunmayı hedefleyerek yılların vermiş olduğu mesleki tecrübenin birikimleriyle hizmet vermektedir. Petrichor Mobili sektöre girdiği ilk günden beri başarı anahtarının koşulsuz müşteri memnuniyeti olduğunu benimsemiş ve müşteri memnuniyeti için sürekli size hizmet sunacak bir altyapı oluşturmuştur. Petrichor Mobili olarak amacımız, yaşam alanlarınızda modern, fonksiyonel, ekonomik ve evinize dair tüm ihtiyaçlarınızı karşılayabilecek ürünleri en uygun şekilde üretmek ve bu amaç için daima kendini yenilemektir. Modern çizgimiz, teknoloji ve kaliteyi bir araya getiren trend koleksiyonlarımız ile gerçek konforu sunmanın mutluluğunu yaşıyoruz. Şık bir konseptte tüm koleksiyonları bir arada sunmak amacı ile geliştirdiğimiz konsept mağazalarımızda ‘’kaliteli yaşam’’ diyerek tüketicimize daha yakın duruyoruz. Dünya tüketim eğilimlerini ve gelişmeleri yakından takip ediyor ve estetiği harmanlayarak ürettiğimiz koleksiyonları, dünyanın bir çok noktasına ulaştırarak global bir vizyon ile büyüyoruz.

Bu e-mail üzerinden bizimle iletişime geçebilirsiniz: petrichormobili@gmail.com
